FROM python:3.7.1-stretch

WORKDIR /usr/src/app
EXPOSE 8888

RUN pip install --no-cache-dir searx==0.14.0 requests pyyaml pygments flask flask-babel lxml python-dateutil pyopenssl
RUN sed -i "s/127.0.0.1/0.0.0.0/g" /usr/local/lib/python3.7/site-packages/searx/settings.yml

CMD [ "python3", "/usr/local/lib/python3.7/site-packages/searx/webapp.py" ]
